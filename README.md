[![Stand With Ukraine](https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/badges/StandWithUkraine.svg)](https://stand-with-ukraine.pp.ua)
# eagles-eye-dataset

The repo is intended for a dataset of AF vehicles and units from the bird's point of view labeled with
vehicle type, model, and bounding boxes.
The dataset is intended to contain images of a low quality, distorted by movement and/or jammers.

### Sources
Centralised:
- [oryx](https://www.oryxspioenkop.com/2022/02/attack-on-europe-documenting-equipment.html)

Decentralised:
- various youtube channels
- other places over the Internet

## Stats
Feel free to run
`python stats.py` or analyse the dataset by yourself. It is located in `data/aggregated_dataset.csv`

Currently, the dataset is of poor size with a bias towards label `0`, as data for the labeled class is naturally
more accessible then the `1`.
```shell
eagles-eye-dataset % python stats.py
Labels: [1 0]
Value counts:
0    479
1    152
Total images: 172
Total annotations: 631
```

Nano YOLOv8 model, trained for 150 ep, barely performs on the dataset, which may satisfy requirements for simple tasks
however tends to overfit and, obviously requires a bigger and more generalised dataset.

## Prediction with YOLOv8
To predict with YOLOv8 trained on the dataset, run:
```shell
python predict.py {file_name}
```
Supported formats: jpeg, jpg, png, mp4.
