import json
import os
import shutil

import cv2 as cv
import numpy as np

selected: str = None
coords = []
image: np.ndarray
is_selected = False


def load_imgs_from(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(img)
    return images


def annotate(event, x, y, flags, param):
    global coords, image, is_selected
    if event == cv.EVENT_LBUTTONDOWN:
        coords = [(x, y)]
    elif event == cv.EVENT_LBUTTONUP:
        coords.append((x, y))
        cv.rectangle(image, coords[0], coords[1], (0, 255, 0), 2)
        cv.imshow('img', image)
        print(f'box: {coords}')
        is_selected = True


def main():
    global image, selected, is_selected
    input_folder = 'img/unanotated'
    output_folder = 'img/raw'
    annotated_folder = 'img/annotated'
    cv.namedWindow('img')
    cv.setMouseCallback('img', annotate)
    records = {}
    for filename in sorted(os.listdir(input_folder)):
        bounding_boxes = []
        records.update({filename: bounding_boxes})
        image = cv.imread(os.path.join(input_folder, filename))
        cv.imshow("img", image)
        cv.moveWindow('img', 200, 0)
        print(f'Showing: {filename}')
        is_finished = False
        while not is_finished:
            key = cv.waitKey(0)
            if key == ord('p'):
                prompt()
            elif key == ord('q'):
                print(f'Finished: {filename}')
                is_finished = True
            else:
                if not selected:
                    prompt()
                print(f'Annotated: {selected} at {coords}')
                bounding_boxes.append({'class': selected, 'box': coords})

        cv.imwrite(os.path.join(annotated_folder, filename), image)
        shutil.move(os.path.join(input_folder, filename), os.path.join(output_folder, filename))
        is_selected = False
        if cv.waitKey(0) == ord('q'):
            break
    cv.destroyAllWindows()
    with open('annotated_dataset.json', 'w') as file:
        json.dump(records, file)


def prompt():
    global selected
    selected = input(f'Which class?\n')


if __name__ == "__main__":
    main()
