import os, re, argparse

import cv2 as cv
from ultralytics import YOLO


def annotate_video(file_name, model, out_path) -> None:
    cap = cv.VideoCapture(file_name)
    out = cv.VideoWriter(out_path, cv.VideoWriter_fourcc(*'MP4V'), 30, (1920, 1080))
    while cap.isOpened():
        success, frame = cap.read()
        if success:
            annotated_frame = model(frame)[0].plot()
            out.write(annotated_frame)
        else:
            break
    cap.release()
    out.release()


def annotate_img(file_name: str, model: YOLO, out_path: str) -> None:
    img = cv.imread(file_name)
    annotated_frame = model(img)[0].plot()
    cv.imwrite(out_path, annotated_frame)


def resolve_annotator(file_extension: str):
    match file_extension:
        case 'jpg' | 'jpeg' | 'png':
            print('Using image annotator')
            return annotate_img
        case 'mp4':
            print('Using video annotator')
            return annotate_video
        case _:
            raise Exception(f'Files in {file_extension} format are not supported.')


def main(file_name: str):
    directory = 'predictions'
    mkdir(directory)
    out_path = f'{directory}/{file_name}'
    model = YOLO('runs/detect/train/weights/best.pt')
    if match := re.search(r'\.(\w+)$', file_name):
        annotator = resolve_annotator(match.group(1))
        annotator(file_name, model, out_path)
        print(f"Annotated {file_name} and persisted at {out_path}.")
    else:
        raise Exception("Please specify the file name with extension.")


def mkdir(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("fileName")
    args = parser.parse_args()
    return args.fileName


if __name__ == "__main__":
    main(parse_args())
