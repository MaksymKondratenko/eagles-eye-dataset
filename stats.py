import pandas as pd
import properties as config


def main():
    df = pd.read_csv(config.AGGREGATED_DATASET_LOCATION)
    counts = df.label.value_counts().to_string(header=False)
    print(f'Labels: {df.label.unique()}')
    print(f'Value counts:\n{counts}')
    print(f'Total images: {df.name.nunique()}')
    print(f'Total annotations: {df.label.count()}')


if __name__ == "__main__":
    main()
