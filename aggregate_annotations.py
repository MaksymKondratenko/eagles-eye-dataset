import os
from typing import TextIO
import properties as config


def aggregate(root: str, stats: TextIO):
    for filename in os.listdir(root):
        with open(f'{root}/{filename}') as file:
            for line in file.readlines():
                write_record(filename, line, stats)
        print(f'Processed {filename}')


def write_titles(stats: TextIO):
    stats.write('name,label,middle_x,middle_y,width,height\n')


def write_record(filename: str, line: str, stats: TextIO):
    cs_line = line.replace(' ', ',')
    name = filename.replace(r'.txt', '')
    stats.write(f'{name},{cs_line}')


def main(root: str):
    with open(config.AGGREGATED_DATASET_LOCATION, 'w') as stats:
        write_titles(stats)
        aggregate(root, stats)
        print('Done')


if __name__ == "__main__":
    main('data/obj_train_data')
