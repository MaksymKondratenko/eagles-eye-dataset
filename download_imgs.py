import os
import cv2 as cv
import pandas as pd
from skimage import io

import properties as props


def read_df():
    return pd.read_csv(props.DS_WITH_IMG_URLS)


def main():
    df = read_df()
    for index, row in df.iterrows():
        link = row.link
        img = io.imread(link)
        if img is not None:
            dirname = row.model.replace(':', '')
            dirs = f'img/{dirname}'
            if not os.path.exists(dirs):
                os.makedirs(dirs)
            path = f'img/{dirname}/{index}.{row.extension}'
            print(f'writing {path}')
            write_img(img, path)


def write_img(img, path):
    img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
    if not cv.imwrite(path, img):
        raise Exception(path)


if __name__ == "__main__":
    main()
