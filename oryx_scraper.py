import bs4
from bs4 import BeautifulSoup as bs
from dataclasses import dataclass
import pandas as pd
import re
import requests
import properties as props


def main():
    entries = []
    for category in find_categories():
        model_text = category.parent.next.next
        model = define_model(model_text)
        link_tags = list(model_text.next_siblings)
        for link_tag in link_tags:
            link, state = parse_link_tag(link_tag)
            if link and state:
                entry = Entry(model=model, link=link, state=state)
                entries.append(entry)
    write_csv(entries)


def find_categories():
    result = requests.get(props.URLS_ORYX)
    doc = bs(result.text, 'html.parser')
    return doc.find_all('img', class_='thumbborder')


def write_csv(entries):
    df = pd.DataFrame(entries)
    df.to_csv(props.DS_WITH_URLS)


def parse_link_tag(link_tag):
    if type(link_tag) == bs4.Tag and link_tag.get('href'):
        img_link = link_tag.get('href')
        link_tag_text = link_tag.contents[0].get_text()
        if pattern := re.search(r",\s?(\w+)", link_tag_text):
            state = pattern.group(1)
            return img_link, state
    return None, None


def define_model(model_text):
    try:
        return re.search(r"\d+\s(.+[^:]):?", model_text.get_text()).group(1).strip()
    except AttributeError:
        print(f"not expected format {model_text.get_text()}")


@dataclass
class Entry:
    model: str
    state: str
    link: str


if __name__ == "__main__":
    main()
