import pandas as pd
import properties as props


def get_extension(df):
    return df['link'].str.extract('\.(?P<ext>\w+)$', expand=True)['ext']


def extract_ds_with_img():
    df = pd.read_csv(props.DS_WITH_URLS)
    df['extension'] = get_extension(df)
    df_w_file_links = df.loc[~df['extension'].isnull()]
    df_w_file_links.to_csv(props.DS_WITH_IMG_URLS, index=False)


def main():
    extract_ds_with_img()


if __name__ == "__main__":
    main()
